<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class FrontController extends VoyagerBaseController
{
    public function createApplication(Request $request)
    {
        $slug = 'applications';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Validate fields with ajax
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        return redirect()->back();

    }
}
