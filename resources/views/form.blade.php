<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="css/app.css" rel="stylesheet">
    <!-- Styles -->
    <style>
        html, body {
            background: rgb(22, 98, 130);
            background: linear-gradient(135deg, rgba(22, 98, 130, 1) 0%, rgba(179, 218, 213, 1) 100%);
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            min-height: 100vh;
            margin: 0;
        }

        .vertical-center {
            display: flex;
            align-items: center;
            min-height: 100vh;
        }

        .form-group {
            position: relative;
        }

        #name, #cv, #email, .custom-file-label {
            padding-left: 45px;
        }

        .form-group:after {
            display: block;
            position: absolute;
            content: '';
            height: 30px;
            width: 1px;
            box-sizing: border-box;
            border-right: 1px dotted #CDCDCD;
            left: 40px;
            bottom: 4px;
            z-index: 2;
        }

        .form-group:before {
            display: block;
            position: absolute;
            left: 15px;
            bottom: 11px;
            font-family: 'Font Awesome 5 Free';
            font-weight: 900;
            -webkit-font-smoothing: antialiased;
            font-style: normal;
            font-variant: normal;
            text-rendering: auto;
            line-height: 1;
            color: #CDCDCD;
            z-index: 2;
        }

        .form-group-name:before {
            content: "\F007";
        }

        .form-group-cv:before {
            content: "\F187";
        }

        .form-group-email:before {
            content: "\F0e0";
        }

        .form-group-message:after {

            bottom: initial;
            top: 35px;
        }

        .form-group-message:before {
            content: "\F27a";
            bottom: initial;
            top: 45px;
        }

        textarea {
            padding-left: 45px !important;
            min-height: 38px;
        }

    </style>
</head>
<body class="">
<div class="container-fluid">
    <div class="row vertical-center align-items-center">
        <form enctype="multipart/form-data" action="{{url('/createApplication')}}" method="post"
              class="col-lg-5 col-md-8 col-sm-10 shadow bg-white mx-auto"
              style="
                -webkit-border-radius: 10px;
                -moz-border-radius: 10px;
                border-radius: 10px;
                padding: 50px;
            ">
            @csrf
            <h1 class="text-center">Job offer</h1>
            <p>If you want to join our team please send your CV</p>
            <div class="form-group form-group-name">
                <label for="name">Your name</label>
                <input name="name" type="text" class="form-control" id="name" placeholder="example: John Doe">
            </div>
            <label for="name">Your CV</label>
            <div class="custom-file form-group form-group-cv">
                <input type="file" class="custom-file-input" id="cv" name="cv[]">
                <label class="custom-file-label" id="cvLabel" for="cv">Choose file</label>
            </div>
            <div class="form-group form-group-email">
                <label for="email">Your email</label>
                <input name="email" type="email" class="form-control" id="email"
                       placeholder="example: JohnDoe@gmail.com">
            </div>
            <div class="form-group form-group-message">
                <label for="message">Your message</label>
                <textarea name="message" id="message" class="form-control"></textarea>
            </div>
            <button class="btn btn-primary btn-block" type="submit">Submit your CV</button>
        </form>
    </div>
</div>
<script>
    let cvInput = document.getElementById("cv");
    let cvLabel = document.getElementById("cvLabel");

    cvInput.onchange = function () {
        let input = this.files[0];
        let text;
        if (input) {
            text = cvInput.value.replace(/^.*[\\\/]/, '');
        } else {
            text = 'Choose file';
        }
        cvLabel.innerHTML = text;
    };
</script>
</body>
</html>
